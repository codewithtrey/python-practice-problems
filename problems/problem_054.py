# Write a function that meets these requirements.
#
# Name:       check_input
# Parameters: one parameter that can hold any value
# Returns:    if the value of the parameter is the
#             string "raise", then it should raise
#             a ValueError. otherwise, it should
#             just return the value of the parameter
#
# Examples
#    * input:   3
#      returns: 3
#    * input:   "this is a string"
#      returns: "this is a string"
#    * input:   "raise"
#      RAISES:  ValueError

# Review. Initially I wasn't clear what the question was asking but not too difficult.

def check_input(any_value):
    if any_value == "raise":
        return ValueError
    else:
        return any_value

test_value = "5"
print(check_input(test_value))
