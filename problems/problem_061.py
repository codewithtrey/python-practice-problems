# Write a function that meets these requirements.
#
# Name:       remove_duplicates
# Parameters: a list of values
# Returns:    a copy of the list removing all
#             duplicate values and keeping the
#             original order
#
# Examples:
#     * input:   [1, 1, 1, 1]
#       returns: [1]
#     * input:   [1, 2, 2, 1]
#       returns: [1, 2]
#     * input:   [1, 3, 3, 20, 3, 2, 2]
#       returns: [1, 3, 20, 2]


#Had to refer to the solution first. Review!!

def remove_duplicates(list):
    test_list = []
    for nums in list:
        if nums not in test_list:
            test_list.append(nums)
    return test_list

test_list1 = [1, 1, 1, 1]
print(remove_duplicates(test_list1))

# def remove_duplicates(values):      # solution
#     output = []                     # solution
#     for value in values:            # solution
#         if value not in output:     # solution
#             output.append(value)    # solution
#     return output                   # solution
