# Complete the reverse_dictionary function which has a
# single parameter that is a dictionary. Return a new
# dictionary that has the original dictionary's values
# for its keys, and the original dictionary's keys for
# its values.
#
# Examples:
#   * input:  {}
#     output: {}
#   * input:  {"key": "value"}
#     output: {"value", "key"}
#   * input:  {"one": 1, "two": 2, "three": 3}
#     output: {1: "one", 2: "two", 3: "three"}


# Let's gooooooo!!! Referred to the Practice Dictionary problem from Learn where we reversed keys and values
def reverse_dictionary(dictionary):
    test_dict = {}
    for key in my_dict:
        new_value = my_dict[key]
        test_dict[new_value] = key

    return test_dict



my_dict = {"key": "value"}

print(reverse_dictionary(my_dict))
