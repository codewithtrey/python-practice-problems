# Complete the sum_of_first_n_even_numbers function which
# accepts a numerical count n and returns the sum of the
# first n even numbers
#
# If the value of the limit is less than 0, then it should
# return None
#
# Examples:
#   * -1 returns None
#   * 0 returns 0
#   * 1 returns 0+2=2
#   * 2 returns 0+2+4=6
#   * 5 returns 0+2+4+6+8+10=30
#
# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

# Review, not understanding why it is sum = sum + i * 2. In my mind it is not taking the first 5 even numbers in that case. I thought you would have to somehow incorporate if i % 2 == 0 to check if the number was even. Confusing problem.
def sum_of_first_n_even_numbers(n):
     # return the sum of the numbers from 0 up to and including limit
    # use the sum function from previous problem
    if n < 0:
        return None
    sum = 0

    for i in range(n + 1):
        sum = sum + i * 2
    return sum





n = 5
print(sum_of_first_n_even_numbers(n))
