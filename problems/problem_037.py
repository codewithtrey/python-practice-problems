# Complete the pad_left function which takes three parameters
#   * a number
#   * the number of characters in the result
#   * a padding character
# and turns the number into a string of the desired length
# by adding the padding character to the left of it
#
# Examples:
#   * number: 10
#     length: 4
#     pad:    "*"
#     result: "**10"
#   * number: 10
#     length: 5
#     pad:    "0"
#     result: "00010"
#   * number: 1000
#     length: 3
#     pad:    "0"
#     result: "1000"
#   * number: 19
#     length: 5
#     pad:    " "
#     result: "   19"

# Review this question, had to refer to the solution before solving! I understood the solution and made a note on the explanation down on line 34.
def pad_left(number, length, pad):
    # pad is adding to the result so that it equals the specified length
    # declare variable (s) and use the str method to change the number paremeter into a string
    s = str(number)
    # while loop defined as while len(s) less than the length parameter
    while len(s) < length:
    # set variable s equal to the parameter pad + s and return s
        s = pad + s # The reason that this works is because in the test case the length argument is 4. On line 8 s is currently "10" which has a length of 2. So in the while loop you are only going to have 2 iterations which is how you arrive at "**10" for a total length of 4. And in order for it to be added to the right it is pad + s, if I did s + pad then it would go to the right and look like "10**".
    return s






num = 10
len_test = 4
pad_test = "*"
print(pad_left(num, len_test, pad_test))
