# Complete the find_second_largest function which accepts
# a list of numerical values and returns the second largest
# in the list
#
# If the list of values is empty, the function should
# return None
#
# If the list of values has only one value, the function
# should return None
#
# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def find_second_largest(values):
    # if the list has one value, return None
    if len(values) <= 1:
        return None
    # sort the list and return the value at index [1]
    values.sort(reverse=True)
    return values[1]

test_list = [1, 2, 3, 4, 5, 7]
print(find_second_largest(test_list))
